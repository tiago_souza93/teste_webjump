const router = require('express').Router();
const DashController = require('@controller/Dash');
const CategoryController  = require('@controller/Category');
const ProductController = require('@controller/Product');
const uploadImage = require('@middleware/uploadImage');
const logger = require('@service/logger');
const multer = require('multer');

router
  .get('/', DashController.index)

  .get('/categorias', CategoryController.index)
  .get('/categorias/nova', CategoryController.new)
  .post('/categorias/create', CategoryController.create)
  .get('/categorias/editar/:id', CategoryController.edit)
  .post('/categorias/salvarAlteracao', CategoryController.saveChanges)
  .get('/categorias/deletar/:id', CategoryController.delete)
  
  .get('/produtos', ProductController.index)
  .get('/produtos/novo', ProductController.new)
  .post('/produtos/create',uploadImage.single('image'), ProductController.create)
  .get('/produtos/editar/:id', ProductController.edit)
  .post('/produtos/salvarAlteracao',uploadImage.single('image'), ProductController.saveChanges)
  .get('/produtos/deletar/:id', ProductController.delete)

module.exports = router;
