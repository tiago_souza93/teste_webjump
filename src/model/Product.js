const mongoose = require('mongoose')

const { Schema } = mongoose;

const ProductSchema = new Schema({
  name: String,
  sku: Number,
  price: Schema.Types.Decimal128,
  quantity: Number,
  categories: [{ type: mongoose.Types.ObjectId, ref: 'category' }],
  description : String,
  image : String
});

module.exports = mongoose.model('product', ProductSchema);
