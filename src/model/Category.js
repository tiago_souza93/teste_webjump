const mongoose = require('mongoose')

const { Schema } = mongoose;

const CategorySchema = new Schema({
  name: String,
  code: Number,
});

module.exports = mongoose.model('category', CategorySchema);
