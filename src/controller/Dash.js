const Product = require('@model/Product')

const DashController = {
  index(req, res) {
    Product.find().populate("categories").lean().then((product) => {
      product.forEach(element => {
        element.price = element.price.toString().replace(".",",");
      });
      res.render("dash/index", {product: product});
    }).catch((err) => {
      req.flash("error_msg", "Erro ao listar produtos.");
      res.redirect('/');
    });
  }

};

module.exports = DashController;
