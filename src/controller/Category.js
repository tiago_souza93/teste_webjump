const Category = require('@model/Category');

const CategoryController = {
    index(req, res) {
      Category.find().lean().then((category) => {
        res.render("category/categories", {category: category});
      }).catch((err) => {
        req.flash("error_msg", "Erro ao listar categorias.");
        res.redirect('/');
      })      
    },

    new(req, res) {
      res.render('category/addCategory');
    },

    create(req, res) {      
      var erros = []
      if(!req.body.name || typeof req.body.name == undefined || req.body.name == null){
        erros.push({text: "Nome inválido"});
      }
      if(!req.body.code || typeof req.body.code == undefined || req.body.code == null){
        erros.push({text: "Código inválido"});
      }
      if(erros.length > 0){
        res.render("category/addCategory", {erros: erros});
      }else{
        const { name, code } = req.body;
        const category = new Category({ name, code });
        category.save((err, category) => {
          if (err) {
            req.flash("error_msg", "Falha ao criar categoria.");
            res.redirect('/categorias');
          }
          req.flash("success_msg", "Categoria criada com sucesso.");
          res.redirect('/categorias');
        });
      }
    },

    edit(req, res) {
      Category.findOne({_id:req.params.id}).lean().then((category) => {
        res.render('category/editCategory', {category:category});
      }).catch((err) => {
        req.flash("error_msg", "Esta categoria não existe.");
        res.redirect('/categorias');
      })      
    },

    saveChanges(req, res) {
      var erros = []
      if(!req.body.name || typeof req.body.name == undefined || req.body.name == null){
        erros.push({text: "Nome inválido"});
      }
      if(!req.body.code || typeof req.body.code == undefined || req.body.code == null){
        erros.push({text: "Código inválido"});
      }
      if(erros.length > 0){
        res.render("category/editCategory", {erros: erros});
      }else{
        
        const product = Category.updateOne({_id:req.body.id}, req.body, (err) => {
          if (err) {
            req.flash("error_msg", "Falha ao editar categoria.");
            res.redirect('/categorias');
          }
          req.flash("success_msg", "Categoria editada com sucesso.");
          res.redirect('/categorias');
        });
      }
    },

    delete(req, res) {
      Category.deleteOne({_id:req.params.id}).then((category) => {
        req.flash("success_msg", "Categoria excluída com sucesso.");
        res.redirect('/categorias');
      }).catch((err) => {
        req.flash("error_msg", "Houve um erro ao deletar categoria.");
        res.redirect('/categorias');
      })      
    },  
};

  module.exports = CategoryController;
  