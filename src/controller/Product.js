const fs = require('fs');
const Product = require('@model/Product');
const Category = require('@model/Category');

const ProductController = {
    index(req, res) {
      Product.find().lean().then((product) => {        
        product.forEach(element => {
          element.price = element.price.toString().replace(".",",");
        })        
        res.render("product/products", {product: product});
      }).catch((err) => {
        req.flash("error_msg", "Erro ao listar produtos.");
        res.redirect('/')
      })      
    },

    new(req, res) {      
      Category.find().lean().then((category) => {
        res.render('product/addProduct', {categories: category});
      }).catch((err) => {
        req.flash("error_msg", "Erro ao listar categorias.");
        res.redirect('/');
      }) 
    },

    create(req, res) {
      var erros = []
      if(!req.body.name || typeof req.body.name == undefined || req.body.name == null){
        erros.push({text: "Nome inválido"});
      }
      if(!req.body.sku || typeof req.body.sku == undefined || req.body.sku == null){
        erros.push({text: "Código inválido"});
      }
      if(!req.body.price || typeof req.body.price == undefined || req.body.price == null){
        erros.push({text: "Preço inválido"});
      }
      if(!req.body.quantity || typeof req.body.quantity == undefined || req.body.quantity == null){
        erros.push({text: "Quantidade inválida"});
      }
      if(!req.body.description || typeof req.body.description == undefined || req.body.description == null){
        erros.push({text: "Descrição inválida"});
      }
      if(req.body.categories == "0"){
        erros.push({text: "Categoria inválida, registre uma categoria."});
      }
      if(erros.length > 0){
        res.render("product/addProduct", {erros: erros});
      }else{

        req.body.image = ''
        if (req.file) {
          req.body.image = req.file.filename;
        }
        const product = new Product(req.body);
        product.save((err) => {
          if (err) {
            req.flash("error_msg", "Falha ao criar produto .");
            res.redirect('/produtos');
          }
          req.flash("success_msg", "Produto criado com sucesso.");
          res.redirect('/produtos');
        });
      }
    },

    edit(req, res) {
      Product.findOne({_id:req.params.id}).populate("categories").lean().then((product) => {        
        Category.find().lean().then((category) => {
          res.render('product/editProduct', {product:product, categories: category});
        }).catch((err) => {
          req.flash("error_msg", "Erro ao listar categorias.");
          res.redirect('/produtos');
        })
        
      }).catch((err) => {
        req.flash("error_msg", "Este produto não existe.");
        res.redirect('/produtos');
      })      
    },
    
    saveChanges(req, res) {
      var erros = []
      if(!req.body.name || typeof req.body.name == undefined || req.body.name == null){
        erros.push({text: "Nome inválido"});
      }
      if(!req.body.sku || typeof req.body.sku == undefined || req.body.sku == null){
        erros.push({text: "Código inválido"});
      }
      if(!req.body.price || typeof req.body.price == undefined || req.body.price == null){
        erros.push({text: "Preço inválido"});
      }
      if(!req.body.quantity || typeof req.body.quantity == undefined || req.body.quantity == null){
        erros.push({text: "Quantidade inválida"});
      }
      if(!req.body.description || typeof req.body.description == undefined || req.body.description == null){
        erros.push({text: "Descrição inválida"});
      }
      if(req.body.categories == "0"){
        erros.push({text: "Categoria inválida, registre uma categoria."});
      }
      if(erros.length > 0){
        res.render("product/editProduct", {erros: erros});
      }else{
        Product.findOne({_id:req.body.id}).then((result) => {    
          const oldImage = result.image;
          req.body.image = oldImage;
          
          if (req.file) {
            fs.unlink("./public/images/product/"+oldImage, function(err){
              if(err){
                return console.log('erro ao excluir arquivo');
              }
            })
            req.body.image = req.file.filename;
          }
          
          const product = Product.updateOne({_id:req.body.id}, req.body, (err) => {
            if (err) {
              req.flash("error_msg", "Falha ao alterar produto .");
              res.redirect('/produtos');
            }
            req.flash("success_msg", "Produto alterado com sucesso.");
            res.redirect('/produtos');
          });
          
        }).catch((err) => {
          req.flash("error_msg", "Erro ao buscar produto.");
          res.redirect('/produtos');
        });
        
      }
    },

    delete(req, res) {
      Product.findOne({_id:req.params.id}).then((result) => {    
        const oldImage = result.image;
        fs.unlink("./public/images/product/"+oldImage, function(err){
          if(err){
            return console.log('erro ao excluir arquivo');
          }
        });

        Product.deleteOne({_id:req.params.id}).then((product) => {        
          req.flash("success_msg", "Produto excluído com sucesso.");
          res.redirect('/produtos');
        }).catch((err) => {
          req.flash("error_msg", "Houve um erro ao deletar produto .");
          res.redirect('/produtos');
        });

      }).catch((err) => {
        req.flash("error_msg", "Erro ao buscar produto.");
        res.redirect('/produtos');
      });

    }
};

  module.exports = ProductController;
  