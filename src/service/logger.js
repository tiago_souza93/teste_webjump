const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
    winston.format.json(),
    winston.format.timestamp(),
    winston.format.printf(info => `[${info.timestamp}] ${info.level} ${info.message}`)
  ),
  defaultMeta: { date: new Date() },
  transports: [
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/all.log' }),
    new winston.transports.Console({ level: 'debug' }),
  ],
});

module.exports = logger;
