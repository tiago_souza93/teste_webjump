const express = require('express');
const sendError = require('@helper/sendError');
const msgFlash = require('@middleware/msgFlash');
const handlebars = require('express-handlebars');
const flash = require('connect-flash');
const session = require("express-session");
const routes = require('./routes');
const app = express();

app.use(session({
    secret: "testewj",
    resave: true,
    saveUninitialized: true
}));
app.use(flash());

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.engine('handlebars', handlebars.engine({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use('*/css',express.static('public/css'));
app.use('*/js',express.static('public/js'));
app.use('*/images',express.static('public/images'));
app.use('*/images/product',express.static('public/images/product'));

app.use(sendError);

app.use(msgFlash);

app.use('/', routes);

module.exports = app;
