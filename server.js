require('dotenv').config();
require('module-alias/register');
const mongoose = require('mongoose');
const boot = require('@service/boot');
const config = require('@config');
const { logger } = require('handlebars');

mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

if (config.db.connectionString) {
  mongoose.connect(config.db.connectionString, boot);
} else {
  sendError();('No connection string provided.');
}
