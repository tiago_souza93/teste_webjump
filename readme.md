# Webjump | Tiago de Oliveira Souza - Vaga Desenvolvedor Back-end NodeJS
### Pré-requisitos
    - NodeJS
    - Express
    
### Sobre o desafio
Mini Sistema de gerenciamento de produtos, com dashboard, formulário de cadastro de produtos e categorias. Com possibilidade de colocar imagem no produto.

### Instalação
-   Clonar projeto 
-   Acesse a pasta do projeto no terminal/cmd e executar: ```npm install``` ou ```yarn install```
-   Para executar digite ```node server.js``` 

### Inicializar sistema
Abra navegador e digite o endereço http://localhost:3000/

### Observação

    Banco já está configurado no MongoDB Atlas.